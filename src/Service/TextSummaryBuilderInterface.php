<?php

namespace Drupal\paragraphs_summary_token\Service;

use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;

/**
 * Interface for the text summary builder.
 */
interface TextSummaryBuilderInterface {

  /**
   * Builds a paragraph summary.
   *
   * @param \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $paragraphs_field
   *   The paragraphs field entity.
   * @param int|null $trim
   *   The length of the text.
   * @param string|null $format
   *   The text summary format.
   *
   * @return string
   *   The summary.
   */
  public function build(EntityReferenceRevisionsFieldItemList $paragraphs_field, ?int $trim = 300, ?string $format = NULL): string;

}
