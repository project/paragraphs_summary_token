<?php

namespace Drupal\paragraphs_summary_token\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\file\Validation\FileValidatorInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\media\MediaInterface;
use Drupal\paragraphs_summary_token\Traits\ParagraphsSummaryTokenTrait;

/**
 * The image builder.
 */
class ImageBuilder implements ImageBuilderInterface {

  use ParagraphsSummaryTokenTrait;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The file url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The file validator service.
   *
   * @var \Drupal\file\Validation\FileValidatorInterface
   */
  protected $fileValidator;

  /**
   * SummaryBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator service.
   * @param \Drupal\file\Validation\FileValidatorInterface $fileValidator
   *   The file validator service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, StreamWrapperManagerInterface $streamWrapperManager, FileUrlGeneratorInterface $fileUrlGenerator, FileValidatorInterface $fileValidator) {
    $this->setFieldStorageConfigStorage($entityTypeManager->getStorage('field_storage_config'));
    $this->fileStorage = $entityTypeManager->getStorage('file');
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->languageManager = $languageManager;
    $this->streamWrapperManager = $streamWrapperManager;
    $this->fileValidator = $fileValidator;
  }

  /**
   * {@inheritdoc}
   */
  public function build(EntityReferenceRevisionsFieldItemList $paragraphs_field, ?ImageStyleInterface $image_style = NULL, string $property_name = 'url'): string {
    $image = $this->retrieveImageFromParagraphsField($paragraphs_field);

    if (!$image instanceof FileInterface) {
      return '';
    }

    return match ($property_name) {
      'width', 'height' => $this->getImageDimensions($image, $property_name, $image_style),
      'uri' => $this->getImageUri($image, $image_style),
      'url' => $this->getImageUrl($image, $image_style),
      'mimetype' => $this->getImageMimeType($image, $image_style),
      'filesize' => $this->getImageFileSize($image, $image_style),
      default => $this->getImageUrl($image, $image_style),
    };
  }

  /**
   * Get the image dimensions of the given image.
   *
   * @param \Drupal\file\FileInterface $file
   *   The image file.
   * @param string $property_name
   *   The property name. Should be width or height.
   * @param \Drupal\image\ImageStyleInterface|null $image_style
   *   The image style when available, else NULL.
   *
   * @return int
   *   The width or height of the image.
   */
  private function getImageDimensions(FileInterface $file, string $property_name, ?ImageStyleInterface $image_style = NULL): int {
    $image = $this->getImageFactory()->get($file->getFileUri());
    $dimensions = [
      'width' => $image->getWidth(),
      'height' => $image->getHeight(),
    ];

    if ($image_style instanceof ImageStyleInterface) {
      $image_style->transformDimensions($dimensions, $file->getFileUri());
    }

    return $dimensions[$property_name];
  }

  /**
   * Get the relative image URL.
   *
   * @param \Drupal\file\FileInterface $file
   *   The image file.
   * @param \Drupal\image\ImageStyleInterface|null $image_style
   *   The image style when available, else NULL.
   *
   * @return string
   *   The relative image URL.
   */
  private function getImageUri(FileInterface $file, ?ImageStyleInterface $image_style = NULL): string {
    $file_uri = $file->getFileUri();

    if ($image_style instanceof ImageStyleInterface) {
      return $image_style->buildUri($file_uri);
    }

    return $file->createFileUrl();
  }

  /**
   * Get the absolute image URL.
   *
   * @param \Drupal\file\FileInterface $file
   *   The image file.
   * @param \Drupal\image\ImageStyleInterface|null $image_style
   *   The image style when available, else NULL.
   *
   * @return string
   *   The absolute image URL.
   */
  private function getImageUrl(FileInterface $file, ?ImageStyleInterface $image_style = NULL): string {
    $file_uri = $file->getFileUri();

    if ($image_style instanceof ImageStyleInterface) {
      return $image_style->buildUrl($file_uri);
    }

    return $file->createFileUrl(FALSE);
  }

  /**
   * Get the image mime type.
   *
   * @param \Drupal\file\FileInterface $file
   *   The image file.
   * @param \Drupal\image\ImageStyleInterface|null $image_style
   *   The image style when available, else NULL.
   *
   * @return string
   *   The mime type of the given file.
   */
  private function getImageMimeType(FileInterface $file, ?ImageStyleInterface $image_style = NULL): string {
    if ($image_style instanceof ImageStyleInterface) {
      $derivative_uri = $image_style->buildUri($file->getFileUri());
      if (!file_exists($derivative_uri)) {
        $image_style->createDerivative($file->getFileUri(), $derivative_uri);
      }

      return $this->getImageFactory()->get($derivative_uri)->getMimeType();
    }

    return $file->getMimeType();
  }

  /**
   * Get the file size.
   *
   * @param \Drupal\file\FileInterface $file
   *   The image file.
   * @param \Drupal\image\ImageStyleInterface|null $image_style
   *   The image style when available, else NULL.
   *
   * @return int
   *   The file size.
   */
  private function getImageFilesize(FileInterface $file, ?ImageStyleInterface $image_style = NULL): int {
    if ($image_style instanceof ImageStyleInterface) {
      $derivative_uri = $image_style->buildUri($file->getFileUri());
      if (!file_exists($derivative_uri)) {
        $image_style->createDerivative($file->getFileUri(), $derivative_uri);
      }

      return $this->getImageFactory()->get($derivative_uri)->getFilesize();
    }

    return $file->getSize();
  }

  /**
   * Get the image from the given paragraphs field.
   *
   * @param \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $paragraphs_field
   *   The paragraphs field.
   *
   * @return \Drupal\file\FileInterface|null
   *   The file when found, else NULL.
   */
  private function retrieveImageFromParagraphsField(EntityReferenceRevisionsFieldItemList $paragraphs_field): ?FileInterface {
    $image = NULL;

    if (!$paragraphs_field->isEmpty()) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      foreach ($paragraphs_field->referencedEntities() as $paragraph) {
        // First, loop over all text_long fields and check if one of those
        // fields contain content.
        $fields = $this->getFieldsByEntityTypeAndFieldType($paragraph->getEntityType(), 'image');
        $fields = array_merge($fields, $this->getEntityReferenceFields($paragraph->getEntityType(), 'media'));
        foreach ($fields as $field) {
          if ($paragraph->hasField($field) &&
            !$paragraph->get($field)->isEmpty()
          ) {
            // Check if the paragraph has a translation.
            if ($paragraph->hasTranslation($language)) {
              $paragraph = $paragraph->getTranslation($language);
            }
            $image = $this->retrieveImageFromField($paragraph->get($field));
            if ($image instanceof FileInterface) {
              break 2;
            }
          }
        }

        // No summary found, check if the paragraph has a reference field to
        // add nested paragraphs.
        foreach ($this->getEntityReferenceFields($paragraph->getEntityType(), 'paragraph', 'entity_reference_revisions') as $paragraphs_field_name) {
          if ($paragraph->hasField($paragraphs_field_name)) {
            $image = $this->retrieveImageFromParagraphsField($paragraph->get($paragraphs_field_name));
            if ($image instanceof FileInterface) {
              break 2;
            }
          }
        }
      }
    }

    return $image;
  }

  /**
   * Retrieve the image from the given field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field that contains an image.
   *
   * @return \Drupal\file\FileInterface|null
   *   The file when found, else NULL.
   */
  private function retrieveImageFromField(FieldItemListInterface $field): ?FileInterface {
    if ($field instanceof FileFieldItemList && $field->entity instanceof FileInterface) {
      $file = $field->entity;
    }
    elseif ($field instanceof EntityReferenceFieldItemListInterface && $field->entity instanceof MediaInterface) {
      $file = $this->retrieveFileFromMediaEntity($field->entity);
    }

    if (!$file instanceof FileInterface) {
      return NULL;
    }

    return $file;
  }

  /**
   * Retrieve the file entity from the given media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return \Drupal\file\FileInterface|null
   *   The file when found, else NULL.
   */
  private function retrieveFileFromMediaEntity(MediaInterface $media): ?FileInterface {
    $source = $media->getSource();

    if ($source->getPluginId() != 'image') {
      return NULL;
    }

    $fid = $source->getSourceFieldValue($media);

    if (!$fid) {
      return NULL;
    }

    $file = $this->fileStorage->load($fid);

    if (!$file instanceof FileInterface) {
      return NULL;
    }

    $errors = $this->fileValidator->validate($file, ['FileIsImage' => []]);

    if ($errors->count() > 0) {
      return NULL;
    }

    return $file;
  }

  /**
   * The image factory.
   *
   * @return \Drupal\Core\Image\ImageFactory
   *   The image factory.
   */
  private function getImageFactory(): ImageFactory {
    // @phpstan-ignore-next-line
    return \Drupal::service('image.factory');
  }

}
