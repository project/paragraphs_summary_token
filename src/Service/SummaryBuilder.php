<?php

namespace Drupal\paragraphs_summary_token\Service;

/**
 * The summary builder.
 *
 * @deprecated in paragraphs_summary_token:2.0.0 and is removed from
 *   paragraphs_summary_token:3.0.0. Use
 *   Drupal\paragraphs_summary_token\Service\TextSummaryBuilder instead.
 *
 * @see https://www.drupal.org/project/paragraphs_summary_token/issues/3205098
 *
 * @see \Drupal\paragraphs_summary_token\Service\TextSummaryBuilder
 */
class SummaryBuilder extends TextSummaryBuilder {

}
