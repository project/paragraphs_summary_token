<?php

namespace Drupal\paragraphs_summary_token\Service;

use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\image\ImageStyleInterface;

/**
 * Interface for the image builder.
 */
interface ImageBuilderInterface {

  /**
   * Retrieves the first image for the given paragraphs field.
   *
   * @param \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $paragraphs_field
   *   The paragraphs field entity.
   * @param \Drupal\image\ImageStyleInterface|null $image_style
   *   The image style.
   * @param string $property_name
   *   The property of the image we want to retrieve.
   *
   * @return string
   *   The image URL.
   */
  public function build(EntityReferenceRevisionsFieldItemList $paragraphs_field, ?ImageStyleInterface $image_style = NULL, string $property_name = 'url'): string;

}
