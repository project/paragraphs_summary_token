<?php

namespace Drupal\paragraphs_summary_token\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\paragraphs_summary_token\Traits\ParagraphsSummaryTokenTrait;

/**
 * Builds a text summary based on paragraphs.
 */
class TextSummaryBuilder implements TextSummaryBuilderInterface {

  use ParagraphsSummaryTokenTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * SummaryBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager) {
    $this->setFieldStorageConfigStorage($entityTypeManager->getStorage('field_storage_config'));
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function build(EntityReferenceRevisionsFieldItemList $paragraphs_field, ?int $trim = 300, ?string $format = NULL): string {
    return text_summary(strip_tags(trim($this->buildSummary($paragraphs_field))), $format, $trim);
  }

  /**
   * Build the summary for the given content entity based on paragraph fields.
   *
   * @param \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $paragraphs_field
   *   The paragraphs field entity.
   *
   * @return string
   *   The summary.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function buildSummary(EntityReferenceRevisionsFieldItemList $paragraphs_field): string {
    $summary = '';

    if (!$paragraphs_field->isEmpty()) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      foreach ($paragraphs_field->referencedEntities() as $paragraph) {
        // First, loop over all text_long fields and check if one of those
        // fields contain content.
        foreach ($this->getFieldsByEntityTypeAndFieldType($paragraph->getEntityType(), 'text_long') as $text_field) {
          if ($paragraph->hasField($text_field) &&
            !$paragraph->get($text_field)->isEmpty()
          ) {
            // Check if the paragraph has a translation.
            if ($paragraph->hasTranslation($language)) {
              $paragraph = $paragraph->getTranslation($language);
            }
            $summary = $paragraph->get($text_field)->value;
            break 2;
          }
        }

        // No summary found, check if the paragraph has a reference field to
        // add nested paragraphs.
        foreach ($this->getEntityReferenceFields($paragraph->getEntityType(), 'paragraph', 'entity_reference_revisions') as $paragraphs_field_name) {
          if ($paragraph->hasField($paragraphs_field_name)) {
            $summary = $this->buildSummary($paragraph->get($paragraphs_field_name));
            if (!empty($summary)) {
              break 2;
            }
          }
        }
      }
    }

    return $summary;
  }

}
