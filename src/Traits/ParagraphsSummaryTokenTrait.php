<?php

namespace Drupal\paragraphs_summary_token\Traits;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Contains helper functions to find the fields for a given entity type.
 */
trait ParagraphsSummaryTokenTrait {

  /**
   * An array that stores the found fields keyed by entity type and field type.
   *
   * @var array
   */
  protected $fields = [];

  /**
   * An array that stores entity reference revision fields.
   *
   * @var array
   */
  protected $entityReferenceFields = [];

  /**
   * The field storage config storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $fieldStorageConfigStorage;

  /**
   * Get all fields for the given entity type of the given field type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to search on.
   * @param string $field_type
   *   The field type to search on.
   *
   * @return array
   *   a list of field names.
   */
  protected function getFieldsByEntityTypeAndFieldType(EntityTypeInterface $entity_type, string $field_type): array {
    if (empty($this->fields[$entity_type->id()][$field_type])) {
      $fields = [];
      /** @var \Drupal\field\FieldStorageConfigInterface[] $paragraph_text_fields_config */
      $field_configs = $this->getFieldStorageConfigStorage()->loadByProperties([
        'entity_type' => $entity_type->id(),
        'type' => $field_type,
      ]);

      foreach ($field_configs as $field_config) {
        $fields[] = $field_config->get('field_name');
      }

      sort($fields);
      $this->fields[$entity_type->id()][$field_type] = $fields;
    }

    return $this->fields[$entity_type->id()][$field_type];
  }

  /**
   * Get all ER or ERR fields for the given entity type and target type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to search on.
   * @param string $target_type
   *   The target type to search on.
   * @param string $field_type
   *   The field type. Should be entity_reference or entity_reference_revisions.
   *
   * @return array
   *   a list of field names.
   */
  protected function getEntityReferenceFields(EntityTypeInterface $entity_type, string $target_type, string $field_type = 'entity_reference'): array {
    if (!in_array($field_type, [
      'entity_reference',
      'entity_reference_revisions',
    ])) {
      throw new \LogicException(sprintf('%s field type not support. Use getFieldsByEntityTypeAndFieldType instead.', $field_type));
    }

    if (empty($this->entityReferenceFields[$entity_type->id()][$field_type][$target_type])) {
      $fields = [];
      /** @var \Drupal\field\FieldStorageConfigInterface[] $paragraph_text_fields_config */
      $field_configs = $this->getFieldStorageConfigStorage()->loadByProperties([
        'entity_type' => $entity_type->id(),
        'type' => $field_type,
      ]);

      foreach ($field_configs as $field_config) {
        $fields[] = $field_config->get('field_name');
      }

      sort($fields);
      $this->entityReferenceFields[$entity_type->id()][$field_type][$target_type] = $fields;
    }

    return $this->entityReferenceFields[$entity_type->id()][$field_type][$target_type];
  }

  /**
   * Gets the field storage config storage.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   *   The field storage config storage.
   */
  protected function getFieldStorageConfigStorage(): ConfigEntityStorageInterface {
    if (!$this->fieldStorageConfigStorage) {
      $this->fieldStorageConfigStorage = \Drupal::service('entity_type.manager')->getStorage('field_storage_config');
    }

    return $this->fieldStorageConfigStorage;
  }

  /**
   * Sets the field storage config storage to use.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $field_storage_config_storage
   *   The field storage config storage.
   *
   * @return $this
   */
  public function setFieldStorageConfigStorage(ConfigEntityStorageInterface $field_storage_config_storage): self {
    $this->fieldStorageConfigStorage = $field_storage_config_storage;
    return $this;
  }

}
