<?php

/**
 * @file
 * Contains tokens for paragraphs_summary_token module.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_token_info_alter().
 */
function paragraphs_summary_token_token_info_alter(&$info) {
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->entityClassImplements(ContentEntityInterface::class)) {
      continue;
    }

    // Make sure a token type exists for this entity.
    $token_type = \Drupal::service('token.entity_mapper')->getTokenTypeForEntityType($entity_type_id);
    if (empty($token_type) || !isset($info['types'][$token_type])) {
      continue;
    }

    $fields = \Drupal::entityTypeManager()->getStorage('field_storage_config')->loadByProperties([
      'entity_type' => $entity_type_id,
      'type' => 'entity_reference_revisions',
    ]);
    foreach ($fields as $field) {
      if (!$field->getSetting('target_type') === 'paragraph') {
        continue;
      }
      $field_token_name = $token_type . '-' . $field->getName();
      $info['tokens']["list<$field_token_name>"]['summary'] = [
        'name' => t('The paragraph summary'),
        'module' => 'paragraphs_summary_token',
        'type' => 'string',
      ];

      if (\Drupal::moduleHandler()->moduleExists('image')) {
        $info['types']['paragraphs_summary_token_image_styles'] = [
          'name' => t('Paragraphs summary token image styles'),
          'needs-data' => 'paragraphs_summary_token_image_styles',
        ];
        $info['tokens']["list<$field_token_name>"]['image'] = [
          'name' => t('The paragraph image'),
          'module' => 'paragraphs_summary_token',
          'type' => 'paragraphs_summary_token_image_styles',
        ];
        $image_styles = ImageStyle::loadMultiple();
        foreach ($image_styles as $image_style_id => $style) {
          $info['tokens']['paragraphs_summary_token_image_styles'][$image_style_id] = [
            'name' => $style->label(),
            'description' => t('Represents the image in the given image style.'),
            'type' => 'string',
          ];
          // Provide tokens for the ImageStyle attributes.
          $info['tokens']['paragraphs_summary_token_image_styles'][$image_style_id]['mimetype'] = [
            'name' => t('MIME type'),
            'description' => t('The MIME type (image/png, image/bmp, etc.) of the image.'),
          ];
          $info['tokens']['paragraphs_summary_token_image_styles'][$image_style_id]['filesize'] = [
            'name' => t('File size'),
            'description' => t('The file size of the image.'),
          ];
          $info['tokens']['paragraphs_summary_token_image_styles'][$image_style_id]['height'] = [
            'name' => t('Height'),
            'description' => t('The height the image, in pixels.'),
          ];
          $info['tokens']['paragraphs_summary_token_image_styles'][$image_style_id]['width'] = [
            'name' => t('Width'),
            'description' => t('The width of the image, in pixels.'),
          ];
          $info['tokens']['paragraphs_summary_token_image_styles'][$image_style_id]['uri'] = [
            'name' => t('URI'),
            'description' => t('The URI to the image.'),
          ];
          $info['tokens']['paragraphs_summary_token_image_styles'][$image_style_id]['url'] = [
            'name' => t('URL'),
            'description' => t('The URL to the image.'),
          ];
        }
      }
    }
  }
}

/**
 * Implements hook_tokens().
 */
function paragraphs_summary_token_tokens($type, $tokens, array $data = [], array $options = []) {
  $return = [];

  if (!empty($data['field_property'])) {
    foreach ($tokens as $name => $original) {
      if (!$data[$data['field_name']] instanceof EntityReferenceFieldItemListInterface) {
        continue;
      }

      if ($name === 'summary') {
        $return[$original] = \Drupal::service('paragraphs_summary_token.text_summary_builder')->build($data[$data['field_name']], 300);
      }
      $name_parts = explode(':', $name);
      if ((isset($name_parts[0]) && $name_parts[0] === 'image')) {
        $image_style = NULL;
        if (isset($name_parts[1]) && \Drupal::moduleHandler()->moduleExists('image')) {
          $image_style = ImageStyle::load($name_parts[1]);
        }
        $property_name = $name_parts[2] ?? 'url';
        $return[$original] = \Drupal::service('paragraphs_summary_token.image_builder')->build($data[$data['field_name']], $image_style, $property_name);
      }
    }
  }

  return $return;
}
