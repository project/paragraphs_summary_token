CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Paragraphs Summary Token module is a helper module that provides 2 tokens.
One token builds a text summary of the given paragraphs field. This token can
be used as e.g. metatag description for entities that use paragraphs to build
the content. The second token tries

How it works:
This module loops over all paragraphs referenced in the given field and checks
if there is a field of the type "text_long" for the text summary or an image
or media field for the image token. The first result will be used to
make the summary. This module also works with nested paragraphs.

REQUIREMENTS
------------
- Paragraphs
- Token

INSTALLATION
------------
Install the "Paragraphs Summary Token" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------
To use this module, there is no extra configuration needed. When you create a
paragraphs field e.g. field_paragraphs, on a content type of your choice, this
module will create a couple of tokens.

## Summary token
The summary token ([node:field_paragraphs:summary]), tries to builds a summary
of the given paragraphs field.

This module loops over all paragraphs referenced in the given field and checks
if there is a field of the type "text_long". The first result will be used to
make the summary. This module also works with nested paragraphs.

## Image token
The image token
([node:field_paragraphs:image] or [node:field_paragraphs:image:large]),
tries to find an image in the paragraphs field. It's possible to pass an
image style.
