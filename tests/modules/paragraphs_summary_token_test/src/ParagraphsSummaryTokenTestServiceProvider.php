<?php

namespace Drupal\paragraphs_summary_token_test;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Overrides the language_manager to allow to override the current language.
 */
class ParagraphsSummaryTokenTestServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('language_manager');
    $definition->setClass(ParagraphsSummaryTokenTestLanguageManager::class);
  }

}
