<?php

namespace Drupal\paragraphs_summary_token_test;

use Drupal\Core\Language\LanguageInterface;
use Drupal\language\ConfigurableLanguageManager;

/**
 * Override language manager that makes it possible to set the current language.
 */
class ParagraphsSummaryTokenTestLanguageManager extends ConfigurableLanguageManager {

  /**
   * Sets current language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language interface.
   * @param string $type
   *   Type interface.
   */
  public function setCurrentLanguage(LanguageInterface $language, $type = LanguageInterface::TYPE_INTERFACE) {
    $this->negotiatedLanguages[$type] = $language;
  }

}
