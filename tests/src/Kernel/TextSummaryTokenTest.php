<?php

namespace Drupal\Tests\paragraphs_summary_token\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\Tests\token\Functional\TokenTestTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Test the text summary token.
 *
 * @group paragraphs_summary_token
 */
class TextSummaryTokenTest extends KernelTestBase {

  use TokenTestTrait;
  use ParagraphsTestBaseTrait;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $bundleInfo;

  /**
   * The content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $contentTranslationManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'field',
    'file',
    'text',
    'entity_reference_revisions',
    'node',
    'user',
    'token',
    'language',
    'content_translation',
    'paragraphs',
    'paragraphs_summary_token',
    'paragraphs_summary_token_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('user');
    $this->container->get('module_handler')->loadInclude('paragraphs', 'install');

    // Add text paragraph.
    $this->addParagraphsType('text');
    $this->addFieldtoParagraphType('text', 'field_body', 'text_long');
    $this->addParagraphedContentType('page');

    // Add container paragraph.
    $this->addParagraphsType('paragraphs_container');
    $this->addParagraphsField('paragraphs_container', 'paragraphs_container_paragraphs', 'paragraph');

    // Add text field with summary to the node.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_text_with_summary',
      'entity_type' => 'node',
      'type' => 'text_with_summary',
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'page',
      'label' => 'Text with summary',
    ]);
    $field->save();

    // Add a second language.
    ConfigurableLanguage::createFromLangcode('nl')->save();

    // Initialize services.
    $this->contentTranslationManager = $this->container->get('content_translation.manager');
    $this->bundleInfo = $this->container->get('entity_type.bundle.info');
    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');
    $this->languageManager = $this->container->get('language_manager');

    // Mark the node page and paragraphs as translatable.
    $this->contentTranslationManager->setEnabled('node', 'page', TRUE);
    $this->contentTranslationManager->setEnabled('paragraph', 'paragraphs_container', TRUE);
    $this->contentTranslationManager->setEnabled('paragraph', 'text', TRUE);
    $this->bundleInfo->clearCachedBundles();
    $this->bundleInfo->getAllBundleInfo();
  }

  /**
   * Test the module with a single paragraph.
   */
  public function testParagraphSummaryTokenWithSingleParagraph(): void {
    $content = $this->getRandomGenerator()->paragraphs(300);
    $paragraph = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>' . $content . '</p>',
    ]);
    $paragraph->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'summary' => text_summary(strip_tags(trim($content)), NULL, 300),
      // Make sure the module doesn't result in an exception when using the
      // image tokens without the image module enabled.
      'image' => '',
      'image:large' => '',
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the module with multiple paragraphs.
   */
  public function testParagraphSummaryTokenWithMultipleParagraphs(): void {
    $paragraph1 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 1</p>',
    ]);
    $paragraph1->save();
    $paragraph2 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 2</p>',
    ]);
    $paragraph2->save();
    $paragraph3 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 3',
    ]);
    $paragraph3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph1, $paragraph2, $paragraph3],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'summary' => 'Paragraph 1',
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the module with nested paragraphs.
   */
  public function testParagraphSummaryTokenWithNestedParagraphs(): void {
    $text_paragraph_1 = Paragraph::create([
      'type' => 'text',
      'field_body' => '',
    ]);
    $text_paragraph_1->save();
    $text_paragraph_2 = Paragraph::create([
      'type' => 'text',
      'field_body' => '',
    ]);
    $text_paragraph_2->save();
    $text_paragraph_3 = Paragraph::create([
      'type' => 'text',
      'field_body' => '',
    ]);
    $text_paragraph_3->save();
    $text_paragraph_4 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 4</p>',
    ]);
    $text_paragraph_3->save();
    $text_paragraph_5 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 5</p>',
    ]);
    $text_paragraph_5->save();

    // Create container that contains the first two text paragraphs.
    $paragraph_1 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $text_paragraph_1,
        $text_paragraph_2,
      ],
    ]);
    $paragraph_1->save();

    $paragraph_2 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $text_paragraph_3,
        $text_paragraph_4,
      ],
    ]);
    $paragraph_2->save();

    $paragraph_3 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $text_paragraph_5,
      ],
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'summary' => 'Paragraph 4',
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the module with symmetric translated paragraphs.
   */
  public function testSymmetricTranslatedParagraphs(): void {
    // Create a text paragraph.
    $paragraph = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>EN content</p>',
    ]);
    // Translate the body field.
    $paragraph = $paragraph->addTranslation('nl');
    $paragraph->set('field_body', '<p>NL content</p>');
    $paragraph->save();

    // Create a node and couple the paragraph.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    // Check the token in the default language.
    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'summary' => 'EN content',
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);

    // Check the token in the second language. The translated field_body should
    // be used.
    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'summary' => 'NL content',
    ];
    $nl_language = $this->languageManager->getLanguage('nl');
    $this->languageManager->setCurrentLanguage($nl_language);
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test this module with asymmetric translated paragraphs.
   */
  public function testAsymmetricTranslatedParagraphs(): void {
    // Create a text paragraph.
    $en_paragraph = Paragraph::create([
      'type' => 'text',
      'field_body' => 'EN content',
    ]);
    $en_paragraph->save();
    // Create a second text paragraph.
    $nl_paragraph = Paragraph::create([
      'type' => 'text',
      'field_body' => 'NL content',
    ]);
    $nl_paragraph->save();

    // Create a node and couple the paragraph.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test EN',
      'field_paragraphs' => [$en_paragraph],
    ]);
    // Add a translation and couple the second paragraph.
    $node = $node->addTranslation('nl');
    $node->set('title', 'Paragraphs Test NL');
    $node->set('field_paragraphs', $nl_paragraph);
    $node->save();

    // Check the token in the default language.
    $node = $this->nodeStorage->load($node->id());
    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'summary' => 'EN content',
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);

    // Check the token in the second language. The translated version should
    // be used.
    $node = $this->nodeStorage->load($node->id());
    $node = $node->getTranslation('nl');
    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'summary' => 'NL content',
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test that this module does not interfere with the default summary token.
   */
  public function testSummaryTokenOnTextFields(): void {
    // Create a node and couple the paragraph.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test EN',
      'field_text_with_summary' => [
        'value' => 'long text value',
        'summary' => 'short summary',
      ],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_text_with_summary',
      'node-field_text_with_summary' => $node->get('field_text_with_summary'),
    ];
    $tokens = [
      'summary' => 'short summary',
    ];
    $this->assertTokens('node:field_text_with_summary', $data, $tokens);
  }

}
