<?php

namespace Drupal\Tests\paragraphs_summary_token\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\Tests\paragraphs_summary_token\Traits\ParagraphsSummaryTokenTestTrait;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Test the image builder service.
 *
 * @group paragraphs_summary_token
 */
class ImageBuilderTest extends KernelTestBase {

  use ParagraphsSummaryTokenTestTrait;
  use ParagraphsTestBaseTrait;

  /**
   * The file url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The image builder.
   *
   * @var \Drupal\paragraphs_summary_token\Service\ImageBuilderInterface
   */
  protected $imageBuilder;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The image style.
   *
   * @var \Drupal\image\Entity\ImageStyle
   */
  protected $imageStyle;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'field',
    'file',
    'image',
    'entity_reference_revisions',
    'media',
    'node',
    'user',
    'token',
    'language',
    'content_translation',
    'paragraphs',
    'paragraphs_summary_token',
    'paragraphs_summary_token_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('media');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->container->get('module_handler')->loadInclude('paragraphs', 'install');

    // Create image style.
    $this->imageStyle = ImageStyle::create(['name' => 'test']);
    $this->imageStyle->addImageEffect([
      'id' => 'image_scale',
      'data' => [
        'width' => 120,
        'height' => 60,
        'upscale' => TRUE,
      ],
    ]);
    $this->imageStyle->save();

    // Add image paragraph.
    $this->addParagraphsType('image');
    $this->addFieldtoParagraphType('image', 'field_image', 'image');

    // Add media paragraph.
    $this->addParagraphsType('media');
    $this->addFieldtoParagraphType('media', 'field_media', 'entity_reference', ['target_type' => 'media']);

    // Add container paragraph.
    $this->addParagraphsType('paragraphs_container');
    $this->addParagraphsField('paragraphs_container', 'paragraphs_container_paragraphs', 'paragraph');

    // Create a content type with paragraphs field.
    $this->addParagraphedContentType('page');

    // Initialize services.
    $this->imageBuilder = $this->container->get('paragraphs_summary_token.image_builder');
    $this->fileUrlGenerator = $this->container->get('file_url_generator');
    $this->fileStorage = $this->container->get('entity_type.manager')->getStorage('file');
    $this->imageFactory = $this->container->get('image.factory');
  }

  /**
   * Test the image summary builder service with a single paragraph.
   */
  public function testImageBuilderWithOneFileParagraph(): void {
    $image = $this->createImageFile();
    $paragraph = Paragraph::create([
      'type' => 'image',
      'field_image' => $image,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($image->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($image->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image summary builder service with multiple paragraphs.
   */
  public function testImageBuilderWithMultipleFileParagraphs(): void {
    $image_1 = $this->createImageFile();
    $paragraph_1 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_1,
    ]);
    $paragraph_1->save();
    $image_2 = $this->createImageFile();
    $paragraph_2 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_2,
    ]);
    $paragraph_2->save();
    $image_3 = $this->createImageFile();
    $paragraph_3 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_3,
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($image_1->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($image_1->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image builder service with nested paragraphs.
   */
  public function testImageBuilderWithNestedFileParagraphs(): void {
    $image_paragraph_1 = Paragraph::create([
      'type' => 'image',
      'field_image' => NULL,
    ]);
    $image_paragraph_1->save();
    $image_paragraph_2 = Paragraph::create([
      'type' => 'image',
      'field_image' => NULL,
    ]);
    $image_paragraph_2->save();
    $image_paragraph_3 = Paragraph::create([
      'type' => 'image',
      'field_image' => NULL,
    ]);
    $image_paragraph_3->save();
    $image_1 = $this->createImageFile();
    $image_paragraph_4 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_1,
    ]);
    $image_paragraph_4->save();
    $image_2 = $this->createImageFile();
    $image_paragraph_5 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_2,
    ]);
    $image_paragraph_5->save();

    // Create container that contains the first two image paragraphs.
    $paragraph_1 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $image_paragraph_1,
        $image_paragraph_2,
      ],
    ]);
    $paragraph_1->save();

    $paragraph_2 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $image_paragraph_3,
        $image_paragraph_4,
      ],
    ]);
    $paragraph_2->save();

    $paragraph_3 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $image_paragraph_5,
      ],
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($image_1->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($image_1->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image summary builder service with a single paragraph.
   */
  public function testImageBuilderWithOneMediaParagraph(): void {
    $media = $this->createImageMedia();
    $paragraph = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image summary builder service with a single paragraph.
   */
  public function testImageBuilderWithMultipleMediaParagraphs(): void {
    $paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $paragraph_1->save();
    $media = $this->createImageMedia();
    $paragraph_2 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph_2->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image summary builder service with a single paragraph.
   */
  public function testImageBuilderWithNestedMediaParagraphs(): void {
    $media_paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $media_paragraph_1->save();
    $media_paragraph_2 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $media_paragraph_2->save();
    $media_paragraph_3 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $media_paragraph_3->save();
    $media_1 = $this->createImageMedia();
    $media_paragraph_4 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media_1,
    ]);
    $media_paragraph_4->save();
    $media_2 = $this->createImageMedia();
    $media_paragraph_5 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media_2,
    ]);
    $media_paragraph_5->save();

    // Create container that contains the first two media paragraphs.
    $paragraph_1 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $media_paragraph_1,
        $media_paragraph_2,
      ],
    ]);
    $paragraph_1->save();

    $paragraph_2 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $media_paragraph_3,
        $media_paragraph_4,
      ],
    ]);
    $paragraph_2->save();

    $paragraph_3 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $media_paragraph_5,
      ],
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($media_1->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($media_1->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image builder with media and image fields combined.
   */
  public function testImageBuilderWithMediaAndImageFieldsCombined(): void {
    $media = $this->createImageMedia();
    $paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph_1->save();

    $image = $this->createImageFile();
    $paragraph_2 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image,
    ]);
    $paragraph_2->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image builder with media and image fields combined.
   */
  public function testImageBuilderWithVideoMedia(): void {
    $file_media = $this->createFileMedia();
    $paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => $file_media,
    ]);
    $paragraph_1->save();

    $image_media = $this->createImageMedia();
    $paragraph_2 = Paragraph::create([
      'type' => 'media',
      'field_media' => $image_media,
    ]);
    $paragraph_2->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2],
    ]);
    $node->save();

    $this->assertEquals(
      $this->fileUrlGenerator->generateAbsoluteString($image_media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );

    $this->assertEquals(
      $this->imageStyle->buildUrl($image_media->get('field_media_image')->entity->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

  /**
   * Test the image properties.
   */
  public function testImageProperties(): void {
    $media = $this->createImageMedia();
    $file_id = $media->getSource()->getSourceFieldValue($media);
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->fileStorage->load($file_id);
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $image = $this->imageFactory->get($file->getFileUri());

    $paragraph = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => $paragraph,
    ]);
    $node->save();

    $this->assertEquals(
      $image->getWidth(),
      $this->imageBuilder->build($node->get('field_paragraphs'), NULL, 'width')
    );
    $this->assertEquals(
      $image->getHeight(),
      $this->imageBuilder->build($node->get('field_paragraphs'), NULL, 'height')
    );
    $this->assertEquals(
      $file->createFileUrl(),
      $this->imageBuilder->build($node->get('field_paragraphs'), NULL, 'uri')
    );
    $this->assertEquals(
      $file->createFileUrl(FALSE),
      $this->imageBuilder->build($node->get('field_paragraphs'), NULL, 'url')
    );
    $this->assertEquals(
      $image->getMimeType(),
      $this->imageBuilder->build($node->get('field_paragraphs'), NULL, 'mimetype')
    );
    $this->assertEquals(
      $image->getFileSize(),
      $this->imageBuilder->build($node->get('field_paragraphs'), NULL, 'filesize')
    );
    $this->assertEquals(
      $file->createFileUrl(FALSE),
      $this->imageBuilder->build($node->get('field_paragraphs'))
    );
  }

  /**
   * Test the image properties with an image style..
   */
  public function testImageStyleProperties(): void {
    $media = $this->createImageMedia();
    $file_id = $media->getSource()->getSourceFieldValue($media);
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->fileStorage->load($file_id);
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $this->imageStyle->createDerivative($file->getFileUri(), $this->imageStyle->buildUri($file->getFileUri()));
    $image = $this->imageFactory->get($this->imageStyle->buildUri($file->getFileUri()));

    $paragraph = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => $paragraph,
    ]);
    $node->save();

    $this->assertEquals(
      $image->getWidth(),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle, 'width')
    );
    $this->assertEquals(
      $image->getHeight(),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle, 'height')
    );
    $this->assertEquals(
      $this->imageStyle->buildUri($file->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle, 'uri')
    );
    $this->assertEquals(
      $this->imageStyle->buildUrl($file->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle, 'url')
    );
    $this->assertEquals(
      $image->getMimeType(),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle, 'mimetype')
    );
    $this->assertEquals(
      $image->getFileSize(),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle, 'filesize')
    );
    $this->assertEquals(
      $this->imageStyle->buildUrl($file->getFileUri()),
      $this->imageBuilder->build($node->get('field_paragraphs'), $this->imageStyle)
    );
  }

}
