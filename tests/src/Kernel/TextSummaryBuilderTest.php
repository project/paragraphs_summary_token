<?php

namespace Drupal\Tests\paragraphs_summary_token\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Test the text summary builder service.
 *
 * @group paragraphs_summary_token
 */
class TextSummaryBuilderTest extends KernelTestBase {

  use ParagraphsTestBaseTrait;

  /**
   * The text summary builder.
   *
   * @var \Drupal\paragraphs_summary_token\Service\SummaryBuilder
   */
  protected $textSummaryBuilder;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'field',
    'file',
    'text',
    'filter',
    'entity_reference_revisions',
    'node',
    'user',
    'token',
    'language',
    'content_translation',
    'paragraphs',
    'paragraphs_summary_token',
    'paragraphs_summary_token_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('user');
    $this->installConfig(['paragraphs_summary_token_test']);
    $this->container->get('module_handler')->loadInclude('paragraphs', 'install');

    // Add text paragraph.
    $this->addParagraphsType('text');
    $this->addFieldtoParagraphType('text', 'field_body', 'text_long');
    $this->addParagraphedContentType('page');

    // Add container paragraph.
    $this->addParagraphsType('paragraphs_container');
    $this->addParagraphsField('paragraphs_container', 'paragraphs_container_paragraphs', 'paragraph');

    // Initialize services.
    $this->textSummaryBuilder = $this->container->get('paragraphs_summary_token.text_summary_builder');
  }

  /**
   * Test the text summary builder service with a single paragraph.
   */
  public function testSummaryBuilderWithOneParagraph(): void {
    $content = $this->getRandomGenerator()->paragraphs(300);
    $paragraph = Paragraph::create([
      'type' => 'text',
      // This invalid HTML is by purpose to test the summary with a filter
      // format.
      'field_body' => '<p>' . $content,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    // Test the default length.
    $this->assertEquals(
      text_summary(strip_tags(trim($content)), NULL, 300),
      $this->textSummaryBuilder->build($node->get('field_paragraphs'))
    );

    // Test the service with a different length provided.
    $this->assertEquals(
      text_summary(strip_tags(trim($content)), NULL, 200),
      $this->textSummaryBuilder->build($node->get('field_paragraphs'), 200)
    );

    // Test the service with no length provided.
    $this->assertEquals(
      text_summary(strip_tags(trim($content))),
      $this->textSummaryBuilder->build($node->get('field_paragraphs'), NULL)
    );

    // Test the service with a filter.
    $this->assertEquals(
      text_summary(strip_tags(trim($content)), 'test_filter', 300),
      $this->textSummaryBuilder->build($node->get('field_paragraphs'), 300, 'test_filter')
    );

    // Test the service with an inexistent filter.
    $this->assertEquals(
      '',
      $this->textSummaryBuilder->build($node->get('field_paragraphs'), 300, 'inexistent_filter')
    );
  }

  /**
   * Test the text summary builder service with multiple paragraph.
   */
  public function testSummaryBuilderWithMultipleParagraphs(): void {
    $paragraph1 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 1</p>',
    ]);
    $paragraph1->save();
    $paragraph2 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 2</p>',
    ]);
    $paragraph2->save();
    $paragraph3 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 3',
    ]);
    $paragraph3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph1, $paragraph2, $paragraph3],
    ]);
    $node->save();

    $this->assertEquals(
      'Paragraph 1',
      $this->textSummaryBuilder->build($node->get('field_paragraphs'))
    );
  }

  /**
   * Test the text summary builder service with nested paragraphs.
   */
  public function testParagraphSummaryTokenWithNestedParagraphs(): void {
    $text_paragraph_1 = Paragraph::create([
      'type' => 'text',
      'field_body' => '',
    ]);
    $text_paragraph_1->save();
    $text_paragraph_2 = Paragraph::create([
      'type' => 'text',
      'field_body' => '',
    ]);
    $text_paragraph_2->save();
    $text_paragraph_3 = Paragraph::create([
      'type' => 'text',
      'field_body' => '',
    ]);
    $text_paragraph_3->save();
    $text_paragraph_4 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 4</p>',
    ]);
    $text_paragraph_3->save();
    $text_paragraph_5 = Paragraph::create([
      'type' => 'text',
      'field_body' => '<p>Paragraph 5</p>',
    ]);
    $text_paragraph_5->save();

    // Create container that contains the first two text paragraphs.
    $paragraph_1 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $text_paragraph_1,
        $text_paragraph_2,
      ],
    ]);
    $paragraph_1->save();

    $paragraph_2 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $text_paragraph_3,
        $text_paragraph_4,
      ],
    ]);
    $paragraph_2->save();

    $paragraph_3 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $text_paragraph_5,
      ],
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $this->assertEquals(
      'Paragraph 4',
      $this->textSummaryBuilder->build($node->get('field_paragraphs'))
    );
  }

}
