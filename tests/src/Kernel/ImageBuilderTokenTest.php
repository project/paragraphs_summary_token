<?php

namespace Drupal\Tests\paragraphs_summary_token\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\Tests\paragraphs_summary_token\Traits\ParagraphsSummaryTokenTestTrait;
use Drupal\Tests\token\Functional\TokenTestTrait;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Test the image tokens.
 *
 * @group paragraphs_summary_token
 */
class ImageBuilderTokenTest extends KernelTestBase {

  use ParagraphsSummaryTokenTestTrait;
  use ParagraphsTestBaseTrait;
  use TokenTestTrait;

  /**
   * The file url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The image style.
   *
   * @var Drupal\image\Entity\ImageStyle
   */
  protected $imageStyle;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'field',
    'file',
    'image',
    'entity_reference_revisions',
    'media',
    'node',
    'user',
    'token',
    'language',
    'content_translation',
    'paragraphs',
    'paragraphs_summary_token',
    'paragraphs_summary_token_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('media');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->container->get('module_handler')->loadInclude('paragraphs', 'install');

    // Create image style.
    $this->imageStyle = ImageStyle::create(['name' => 'test']);
    $this->imageStyle->addImageEffect([
      'id' => 'image_scale',
      'data' => [
        'width' => 120,
        'height' => 60,
        'upscale' => TRUE,
      ],
    ]);
    $this->imageStyle->save();

    // Add image paragraph.
    $this->addParagraphsType('image');
    $this->addFieldtoParagraphType('image', 'field_image', 'image');

    // Add media paragraph.
    $this->addParagraphsType('media');
    $this->addFieldtoParagraphType('media', 'field_media', 'entity_reference', ['target_type' => 'media']);

    // Add container paragraph.
    $this->addParagraphsType('paragraphs_container');
    $this->addParagraphsField('paragraphs_container', 'paragraphs_container_paragraphs', 'paragraph');

    // Create a content type with paragraphs field.
    $this->addParagraphedContentType('page');

    $this->fileUrlGenerator = $this->container->get('file_url_generator');
    $this->fileStorage = $this->container->get('entity_type.manager')->getStorage('file');
    $this->imageFactory = $this->container->get('image.factory');
  }

  /**
   * Test the image token with one file paragraph.
   */
  public function testImageTokenWithOneFileParagraph(): void {
    $image = $this->createImageFile();
    $paragraph = Paragraph::create([
      'type' => 'image',
      'field_image' => $image,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($image->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($image->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the image token with multiple paragraphs.
   */
  public function testImageTokenWithMultipleFileParagraphs(): void {
    $image_1 = $this->createImageFile();
    $paragraph_1 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_1,
    ]);
    $paragraph_1->save();
    $image_2 = $this->createImageFile();
    $paragraph_2 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_2,
    ]);
    $paragraph_2->save();
    $image_3 = $this->createImageFile();
    $paragraph_3 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_3,
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($image_1->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($image_1->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the image token with nested paragraphs.
   */
  public function testImageTokenWithNestedFileParagraphs(): void {
    $image_paragraph_1 = Paragraph::create([
      'type' => 'image',
      'field_image' => NULL,
    ]);
    $image_paragraph_1->save();
    $image_paragraph_2 = Paragraph::create([
      'type' => 'image',
      'field_image' => NULL,
    ]);
    $image_paragraph_2->save();
    $image_paragraph_3 = Paragraph::create([
      'type' => 'image',
      'field_image' => NULL,
    ]);
    $image_paragraph_3->save();
    $image_1 = $this->createImageFile();
    $image_paragraph_4 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_1,
    ]);
    $image_paragraph_4->save();
    $image_2 = $this->createImageFile();
    $image_paragraph_5 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image_2,
    ]);
    $image_paragraph_5->save();

    // Create container that contains the first two image paragraphs.
    $paragraph_1 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $image_paragraph_1,
        $image_paragraph_2,
      ],
    ]);
    $paragraph_1->save();

    $paragraph_2 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $image_paragraph_3,
        $image_paragraph_4,
      ],
    ]);
    $paragraph_2->save();

    $paragraph_3 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $image_paragraph_5,
      ],
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($image_1->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($image_1->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the image token with a single paragraph.
   */
  public function testImageTokenWithOneMediaParagraph(): void {
    $media = $this->createImageMedia();
    $paragraph = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($media->get('field_media_image')->entity->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($media->get('field_media_image')->entity->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the image token with a single paragraph.
   */
  public function testImageTokenWithMultipleMediaParagraphs(): void {
    $paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $paragraph_1->save();
    $media = $this->createImageMedia();
    $paragraph_2 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph_2->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($media->get('field_media_image')->entity->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($media->get('field_media_image')->entity->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the image token with a single paragraph.
   */
  public function testImageTokenWithNestedMediaParagraphs(): void {
    $media_paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $media_paragraph_1->save();
    $media_paragraph_2 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $media_paragraph_2->save();
    $media_paragraph_3 = Paragraph::create([
      'type' => 'media',
      'field_media' => NULL,
    ]);
    $media_paragraph_3->save();
    $media_1 = $this->createImageMedia();
    $media_paragraph_4 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media_1,
    ]);
    $media_paragraph_4->save();
    $media_2 = $this->createImageMedia();
    $media_paragraph_5 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media_2,
    ]);
    $media_paragraph_5->save();

    // Create container that contains the first two media paragraphs.
    $paragraph_1 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $media_paragraph_1,
        $media_paragraph_2,
      ],
    ]);
    $paragraph_1->save();

    $paragraph_2 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $media_paragraph_3,
        $media_paragraph_4,
      ],
    ]);
    $paragraph_2->save();

    $paragraph_3 = Paragraph::create([
      'type' => 'paragraphs_container',
      'paragraphs_container_paragraphs' => [
        $media_paragraph_5,
      ],
    ]);
    $paragraph_3->save();

    // Add test content with paragraph container.
    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2, $paragraph_3],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($media_1->get('field_media_image')->entity->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($media_1->get('field_media_image')->entity->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the image token with media and image fields combined.
   */
  public function testImageTokenWithMediaAndImageFieldsCombined(): void {
    $media = $this->createImageMedia();
    $paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph_1->save();

    $image = $this->createImageFile();
    $paragraph_2 = Paragraph::create([
      'type' => 'image',
      'field_image' => $image,
    ]);
    $paragraph_2->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($media->get('field_media_image')->entity->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($media->get('field_media_image')->entity->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Test the image token with media and image fields combined.
   */
  public function testImageTokenWithVideoMedia(): void {
    $file_media = $this->createFileMedia();
    $paragraph_1 = Paragraph::create([
      'type' => 'media',
      'field_media' => $file_media,
    ]);
    $paragraph_1->save();

    $image_media = $this->createImageMedia();
    $paragraph_2 = Paragraph::create([
      'type' => 'media',
      'field_media' => $image_media,
    ]);
    $paragraph_2->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => [$paragraph_1, $paragraph_2],
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image' => $this->fileUrlGenerator->generateAbsoluteString($image_media->get('field_media_image')->entity->getFileUri()),
      'image:test' => $this->imageStyle->buildUrl($image_media->get('field_media_image')->entity->getFileUri()),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

  /**
   * Tests the image property tokens.
   */
  public function testImagePropertyTokens(): void {
    $media = $this->createImageMedia();
    $file_id = $media->getSource()->getSourceFieldValue($media);
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->fileStorage->load($file_id);
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $this->imageStyle->createDerivative($file->getFileUri(), $this->imageStyle->buildUri($file->getFileUri()));
    $image = $this->imageFactory->get($this->imageStyle->buildUri($file->getFileUri()));

    $paragraph = Paragraph::create([
      'type' => 'media',
      'field_media' => $media,
    ]);
    $paragraph->save();

    $node = Node::create([
      'type' => 'page',
      'title' => 'Paragraphs Test',
      'field_paragraphs' => $paragraph,
    ]);
    $node->save();

    $data = [
      'field_property' => TRUE,
      'field_name' => 'node-field_paragraphs',
      'node-field_paragraphs' => $node->get('field_paragraphs'),
    ];
    $tokens = [
      'image:test:width' => $image->getWidth(),
      'image:test:height' => $image->getHeight(),
      'image:test:uri' => $this->imageStyle->buildUri($file->getFileUri()),
      'image:test:url' => $this->imageStyle->buildUrl($file->getFileUri()),
      'image:test:mimetype' => $image->getMimeType(),
      'image:test:filesize' => $image->getFileSize(),
    ];
    $this->assertTokens('node:field_paragraphs', $data, $tokens);
  }

}
