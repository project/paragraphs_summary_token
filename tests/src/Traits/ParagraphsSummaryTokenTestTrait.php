<?php

namespace Drupal\Tests\paragraphs_summary_token\Traits;

use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Helper trait for paragraphs summary used in testing.
 */
trait ParagraphsSummaryTokenTestTrait {

  use MediaTypeCreationTrait;

  /**
   * The image media type.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $imageMediaType;

  /**
   * The file media type.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $fileMediaType;

  /**
   * Generates an image file.
   *
   * @return \Drupal\file\FileInterface
   *   An image file.
   */
  protected function createImageFile(): FileInterface {
    $files = $this->getTestFiles('image');
    $file = reset($files);
    $file = File::create([
      'uri' => $file->uri,
    ]);
    $file->save();
    return $file;
  }

  /**
   * Generates a text file.
   *
   * @return \Drupal\file\FileInterface
   *   A text file.
   */
  protected function createTextFile(): FileInterface {
    $files = $this->getTestFiles('text');
    $file = reset($files);
    $file = File::create([
      'uri' => $file->uri,
    ]);
    $file->save();
    return $file;
  }

  /**
   * Create a media entity linked to an image.
   *
   * @return \Drupal\Media\MediaInterface
   *   A media entity linked to an image.
   */
  protected function createImageMedia(): MediaInterface {
    if (!$this->imageMediaType instanceof MediaTypeInterface) {
      $this->imageMediaType = $this->createMediaType('image');
    }

    $source = $this->imageMediaType->getSource();
    $source_field = $source->getSourceFieldDefinition($this->imageMediaType)->getName();
    $media = Media::create([
      'name' => $this->randomMachineName(),
      'bundle' => $this->imageMediaType->id(),
      $source_field => $this->createImageFile()->id(),
    ]);
    $media->save();
    return $media;
  }

  /**
   * Create a media entity linked to a file.
   *
   * @return \Drupal\Media\MediaInterface
   *   A media entity linked to a file.
   */
  protected function createFileMedia(): MediaInterface {
    if (!$this->fileMediaType instanceof MediaTypeInterface) {
      $this->fileMediaType = $this->createMediaType('file');
    }

    $source = $this->fileMediaType->getSource();
    $source_field = $source->getSourceFieldDefinition($this->fileMediaType)->getName();
    $media = Media::create([
      'name' => $this->randomMachineName(),
      'bundle' => $this->fileMediaType->id(),
      $source_field => $this->createTextFile()->id(),
    ]);
    $media->save();
    return $media;
  }

}
